
/*---------------------------------------------------------------
  SQL DB BACKUP 15.02.2023 02:44 
  HOST: localhost
  DATABASE: fuwafuwa_zamzam_elearning
  TABLES: *
  ---------------------------------------------------------------*/

/*---------------------------------------------------------------
  TABLE: `absen_harian`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `absen_harian`;
CREATE TABLE `absen_harian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `level` varchar(20) DEFAULT 'siswa',
  `hadir` date DEFAULT NULL,
  `waktu` time DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `bulan` tinyint(4) DEFAULT NULL,
  `hari` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3713 DEFAULT CHARSET=utf8mb4;
INSERT INTO `absen_harian` VALUES   ('3627','448','siswa','2022-12-19','15:32:05','2022','12','19');
INSERT INTO `absen_harian` VALUES ('3628','1','siswa','2023-01-29','13:37:47','2023','1','29');
INSERT INTO `absen_harian` VALUES ('3629','344','guru','2023-02-11','14:59:04','2023','2','11');
INSERT INTO `absen_harian` VALUES ('3630','343','guru','2023-02-11','15:06:00','2023','2','11');
INSERT INTO `absen_harian` VALUES ('3631','345','guru','2023-02-11','15:07:51','2023','2','11');
INSERT INTO `absen_harian` VALUES ('3632','334','guru','2023-02-11','15:11:59','2023','2','11');
INSERT INTO `absen_harian` VALUES ('3633','341','guru','2023-02-11','15:32:06','2023','2','11');
INSERT INTO `absen_harian` VALUES ('3634','2','siswa','2023-02-11','15:35:33','2023','2','11');
INSERT INTO `absen_harian` VALUES ('3635','2','siswa','2023-02-12','18:32:50','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3636','10','siswa','2023-02-12','19:01:12','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3637','11','siswa','2023-02-12','19:02:58','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3638','20','siswa','2023-02-12','19:03:02','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3639','9','siswa','2023-02-12','19:03:08','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3640','5','siswa','2023-02-12','19:03:24','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3641','7','siswa','2023-02-12','19:04:09','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3642','15','siswa','2023-02-12','19:05:25','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3643','6','siswa','2023-02-12','19:05:49','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3644','26','siswa','2023-02-12','19:06:25','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3645','21','siswa','2023-02-12','19:06:37','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3646','25','siswa','2023-02-12','19:07:14','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3647','29','siswa','2023-02-12','19:07:33','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3648','28','siswa','2023-02-12','19:07:53','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3649','33','siswa','2023-02-12','19:07:59','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3650','14','siswa','2023-02-12','19:08:11','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3651','23','siswa','2023-02-12','19:08:37','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3652','334','guru','2023-02-12','19:09:50','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3653','332','guru','2023-02-12','19:14:13','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3654','30','siswa','2023-02-12','19:19:56','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3655','22','siswa','2023-02-12','19:21:33','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3656','34','siswa','2023-02-12','19:21:43','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3657','43','siswa','2023-02-12','19:21:56','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3658','36','siswa','2023-02-12','19:22:02','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3659','45','siswa','2023-02-12','19:23:02','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3660','4','siswa','2023-02-12','19:23:56','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3661','40','siswa','2023-02-12','19:24:27','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3662','48','siswa','2023-02-12','19:24:30','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3663','32','siswa','2023-02-12','19:24:44','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3664','13','siswa','2023-02-12','19:26:05','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3665','37','siswa','2023-02-12','19:29:41','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3666','49','siswa','2023-02-12','19:29:48','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3667','39','siswa','2023-02-12','19:29:52','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3668','52','siswa','2023-02-12','19:30:55','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3669','12','siswa','2023-02-12','19:41:29','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3670','56','siswa','2023-02-12','19:44:36','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3671','59','siswa','2023-02-12','20:01:29','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3672','41','siswa','2023-02-12','20:02:46','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3673','57','siswa','2023-02-12','20:05:14','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3674','60','siswa','2023-02-12','20:12:49','2023','2','12');
INSERT INTO `absen_harian` VALUES ('3675','11','siswa','2023-02-13','07:00:28','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3676','33','siswa','2023-02-13','07:13:39','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3677','34','siswa','2023-02-13','07:33:56','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3678','21','siswa','2023-02-13','07:42:58','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3679','6','siswa','2023-02-13','08:04:19','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3680','14','siswa','2023-02-13','09:11:38','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3681','15','siswa','2023-02-13','09:21:05','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3682','53','siswa','2023-02-13','10:36:03','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3683','52','siswa','2023-02-13','10:58:04','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3684','13','siswa','2023-02-13','11:03:33','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3685','9','siswa','2023-02-13','11:44:56','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3686','23','siswa','2023-02-13','11:51:01','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3687','62','siswa','2023-02-13','11:58:11','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3688','64','siswa','2023-02-13','12:04:30','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3689','37','siswa','2023-02-13','12:12:58','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3690','65','siswa','2023-02-13','12:39:06','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3691','29','siswa','2023-02-13','14:11:20','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3692','32','siswa','2023-02-13','17:41:43','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3693','2','siswa','2023-02-13','18:10:05','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3694','61','siswa','2023-02-13','19:12:08','2023','2','13');
INSERT INTO `absen_harian` VALUES ('3695','54','siswa','2023-02-14','05:44:37','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3696','56','siswa','2023-02-14','05:45:25','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3697','33','siswa','2023-02-14','07:32:30','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3698','11','siswa','2023-02-14','08:00:27','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3699','63','siswa','2023-02-14','08:24:39','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3700','25','siswa','2023-02-14','08:38:43','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3701','37','siswa','2023-02-14','09:23:53','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3702','23','siswa','2023-02-14','10:38:04','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3703','332','guru','2023-02-14','10:40:53','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3704','53','siswa','2023-02-14','11:40:07','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3705','20','siswa','2023-02-14','13:21:45','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3706','13','siswa','2023-02-14','14:04:09','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3707','15','siswa','2023-02-14','14:59:38','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3708','14','siswa','2023-02-14','14:59:51','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3709','21','siswa','2023-02-14','16:26:00','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3710','9','siswa','2023-02-14','18:26:59','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3711','61','siswa','2023-02-14','22:02:52','2023','2','14');
INSERT INTO `absen_harian` VALUES ('3712','34','siswa','2023-02-15','02:41:27','2023','2','15');

/*---------------------------------------------------------------
  TABLE: `agama`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `agama`;
CREATE TABLE `agama` (
  `agamaku` varchar(25) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `agama` VALUES   ('Islam');
INSERT INTO `agama` VALUES ('Kristen Katolik');
INSERT INTO `agama` VALUES ('Kristen Protestan');
INSERT INTO `agama` VALUES ('Hindu');
INSERT INTO `agama` VALUES ('Budha');
INSERT INTO `agama` VALUES ('Konghucu');
INSERT INTO `agama` VALUES ('Islam');
INSERT INTO `agama` VALUES ('Kristen Katolik');
INSERT INTO `agama` VALUES ('Kristen Protestan');
INSERT INTO `agama` VALUES ('Hindu');
INSERT INTO `agama` VALUES ('Buddha');
INSERT INTO `agama` VALUES ('Kong Hu Cu');
INSERT INTO `agama` VALUES ('Kristen');
INSERT INTO `agama` VALUES ('Islam');
INSERT INTO `agama` VALUES ('Kristen Katolik');
INSERT INTO `agama` VALUES ('Kristen Protestan');
INSERT INTO `agama` VALUES ('Hindu');
INSERT INTO `agama` VALUES ('Buddha');
INSERT INTO `agama` VALUES ('Kong Hu Cu');
INSERT INTO `agama` VALUES ('Kristen');

/*---------------------------------------------------------------
  TABLE: `berita`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `berita`;
CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `id_mapel` int(10) NOT NULL,
  `sesi` varchar(10) NOT NULL,
  `ruang` varchar(20) NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `ikut` varchar(10) DEFAULT NULL,
  `susulan` varchar(10) DEFAULT NULL,
  `no_susulan` text DEFAULT NULL,
  `mulai` varchar(10) DEFAULT NULL,
  `selesai` varchar(10) DEFAULT NULL,
  `nama_proktor` varchar(50) DEFAULT NULL,
  `nip_proktor` varchar(50) DEFAULT NULL,
  `nama_pengawas` varchar(50) DEFAULT NULL,
  `nip_pengawas` varchar(50) DEFAULT NULL,
  `catatan` text DEFAULT NULL,
  `tgl_ujian` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_berita`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

/*---------------------------------------------------------------
  TABLE: `file_pendukung`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `file_pendukung`;
CREATE TABLE `file_pendukung` (
  `id_file` int(11) NOT NULL AUTO_INCREMENT,
  `id_mapel` int(11) DEFAULT 0,
  `nama_file` varchar(50) DEFAULT NULL,
  `status_file` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8;

/*---------------------------------------------------------------
  TABLE: `jawaban`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `jawaban`;
CREATE TABLE `jawaban` (
  `id_jawaban` int(11) NOT NULL AUTO_INCREMENT,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `id_ujian` int(11) NOT NULL,
  `jawaban` char(1) DEFAULT NULL,
  `jawabx` char(1) DEFAULT NULL,
  `jenis` int(1) NOT NULL,
  `esai` text DEFAULT NULL,
  `nilai_esai` int(5) NOT NULL DEFAULT 0,
  `ragu` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_jawaban`)
) ENGINE=InnoDB AUTO_INCREMENT=171859 DEFAULT CHARSET=latin1;

/*---------------------------------------------------------------
  TABLE: `jawaban_tugas`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `jawaban_tugas`;
CREATE TABLE `jawaban_tugas` (
  `id_jawaban` int(11) NOT NULL AUTO_INCREMENT,
  `id_siswa` int(11) DEFAULT NULL,
  `id_tugas` int(11) DEFAULT NULL,
  `jawaban` longtext DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `tgl_dikerjakan` datetime DEFAULT NULL,
  `tgl_update` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `nilai` varchar(5) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_jawaban`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
INSERT INTO `jawaban_tugas` VALUES   ('2','1','7','<p>1. Pensil adalah alat tulis</p>','7_1.jpg',NULL,'2023-01-29 13:53:31','85',NULL);
INSERT INTO `jawaban_tugas` VALUES ('4','2','9','<p>6</p>',NULL,NULL,'2023-02-11 16:52:51','100',NULL);
INSERT INTO `jawaban_tugas` VALUES ('5','2','10','<p>YAAAAA</p><p><br></p>','10_2.png',NULL,'2023-02-11 17:00:15',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('6','1','12','<p>1. Kesan saya selama mengikuti kegiatan pelatihan e-learning PKBM Zamzam cuku seru dan dashboardnya sangat friendly</p>','12_1.png',NULL,'2023-02-12 19:32:00',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('7','37','12','','12_37.png',NULL,'2023-02-12 19:32:03',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('8','38','12','Keren ih e-learning ya, selamat ya PKBM Zam zam, suksess selaluu',NULL,NULL,'2023-02-12 19:32:04',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('9','49','12','<p>1. Sangan menyenangkan</p><p>2.&nbsp;<img src=\"https://pkbm.zamzam.sch.id/elearning/temp/1676205139a8baa56554f96369ab93e4f3bb068c22.JPG\" style=\"width: 1027px;\"></p>',NULL,NULL,'2023-02-12 19:32:36',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('10','23','12','<p>Sangat senang sekali PKBM ZAMZAM punya e learning, inovatif sekali...</p>','12_23.png',NULL,'2023-02-12 19:33:12',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('11','27','12','<p>Semoga aplikasinya berjalan lancar dan maksimal Terima kasih PKBM ZAM2</p>','12_27.jpg',NULL,'2023-02-12 19:33:20',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('12','48','12','',NULL,NULL,'2023-02-12 19:33:38',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('13','24','12','',NULL,NULL,'2023-02-12 19:33:52',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('14','25','12','<p>Mantap, semoga semakin mudah dg sistem yang baru, </p>',NULL,NULL,'2023-02-14 08:39:04',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('15','11','12','<p>1. Kesan saya selama mengikuti kegiatan pelatihan e-learning PKBM Zamzam qseru dan dashboaranya sangat friendly<br></p>',NULL,NULL,'2023-02-12 19:39:00',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('16','13','12','cukup mudah, semoga bisa dilaksanakan dan dioperasikan oleh anak-anak&nbsp;','12_13.jpg',NULL,'2023-02-12 19:43:04',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('17','39','12','<p>1. Menyenangkan.&nbsp;</p><p>2.&nbsp;<img src=\"https://pkbm.zamzam.sch.id/elearning/temp/167620582431fefc0e570cb3860f2a6d4b38c6490d.JPG\" style=\"width: 133px;\"></p>',NULL,NULL,'2023-02-12 19:43:47',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('18','53','12','<p>Sangat membantu sekali untuk pengumpulan tugas saat ada aplikasi seperti ini.&nbsp; Semoga kerja keras para tim penyusun aplikasi E-learning mendapat balasan pahala dari Allah.&nbsp; Amiinn...<img style=\"width: 25%; float: right;\" src=\"https://pkbm.zamzam.sch.id/elearning/temp/167620595502522a2b2726fb0a03bb19f2d8d9524d.png\" class=\"note-float-right\"><br></p><p><br></p><p><br></p><p><br></p>',NULL,NULL,'2023-02-12 19:46:51',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('19','59','12','<p>alhamdulillah pkbm sdh punya e learning sendiri. sukses selau</p>','12_59.png',NULL,'2023-02-12 20:03:06',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('20','21','12','<p>Kesan:</p><p>Bagus dan cepat loading nya.</p><p><br></p><p>Saran:</p><p>Ditambahkan fitur edit profil, agar bisa mengganti foto kucing ???? ^^</p><p>Laman ini diberi cover, mungkin \"Selamat datang di PKBM Zamzam\". Laman yang dimaksud adalah: https://pkbm.zamzam.sch.id/</p><p><br></p>','12_21.jpg',NULL,'2023-02-13 07:55:54',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('21','14','12','<p>Kesan pertama saya di learning adalah lebih lengkap Dari setara daring Dan setalah ujian bisa langsung memperlihatkan nilai&nbsp;<img src=\"https://pkbm.zamzam.sch.id/elearning/temp/1676254798013d407166ec4fa56eb1e1f8cbe183b9.jpg\" style=\"width: 302px;\"></p>',NULL,NULL,'2023-02-13 09:20:08',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('22','15','12','<p>Begitu says masuk says merasa ini lebih baik daripada setara daring<img src=\"https://pkbm.zamzam.sch.id/elearning/temp/16762550356c4b761a28b734fe93831e3fb400ce87.jpg\" style=\"width: 302px;\"></p>',NULL,NULL,'2023-02-13 09:24:06',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('23','52','12','<p>1. Aku suka sekali.</p>','12_52.jpg',NULL,'2023-02-13 11:02:03',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('24','62','12','<p>Bagus</p><p>Pingin ngubah foto profil tapi ga bisa</p><p><br></p><p><img src=\"https://pkbm.zamzam.sch.id/elearning/temp/167626449602522a2b2726fb0a03bb19f2d8d9524d.jpg\" style=\"width: 268px;\"><br></p>',NULL,NULL,'2023-02-13 12:01:51',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('25','64','12','<p>Pakai ini sukaaa banget</p><p><br></p><p><br></p>','12_64.jpg',NULL,'2023-02-13 12:09:39',NULL,NULL);
INSERT INTO `jawaban_tugas` VALUES ('26','29','12','<p>1. E learning pkbm zam zam secara penampilan lebih baik dari setara daring. Secara aplikasi lebih mudah dipahami dan easy using. Semoga kedepannya jaringan aman tidak sering error. </p>','12_29.jpg',NULL,'2023-02-13 14:17:30',NULL,NULL);

/*---------------------------------------------------------------
  TABLE: `jenis`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `jenis`;
CREATE TABLE `jenis` (
  `id_jenis` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `jenis` VALUES   ('PTS','Penilaian Tengah Semester','aktif');
INSERT INTO `jenis` VALUES ('USBK','Ujian Sekolah','tidak');

/*---------------------------------------------------------------
  TABLE: `jurusan`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `jurusan`;
CREATE TABLE `jurusan` (
  `jurusan_id` varchar(25) NOT NULL,
  `nama_jurusan_sp` varchar(100) DEFAULT NULL,
  `kurikulum` varchar(120) DEFAULT NULL,
  `jurusan_sp_id` varchar(50) DEFAULT NULL,
  `kurikulum_id` varchar(20) DEFAULT NULL,
  `sekolah_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`jurusan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*---------------------------------------------------------------
  TABLE: `kelas`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `kelas`;
CREATE TABLE `kelas` (
  `id_kelas` varchar(11) NOT NULL,
  `level` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `rombongan_belajar_id` varchar(50) DEFAULT NULL,
  `semester_id` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `kelas` VALUES   ('X NA','X','X NA',NULL,NULL);
INSERT INTO `kelas` VALUES ('X NB','X','X NB',NULL,NULL);
INSERT INTO `kelas` VALUES ('X TA','X','X TA',NULL,NULL);
INSERT INTO `kelas` VALUES ('X TB','X','X TB',NULL,NULL);
INSERT INTO `kelas` VALUES ('XI NA','XI','XI NA',NULL,NULL);
INSERT INTO `kelas` VALUES ('XI NB','XI ','XI NB',NULL,NULL);
INSERT INTO `kelas` VALUES ('XI NC','XI','XI NC',NULL,NULL);
INSERT INTO `kelas` VALUES ('XI TA','XI','XI TA',NULL,NULL);
INSERT INTO `kelas` VALUES ('XI TB','XI','XI TB',NULL,NULL);
INSERT INTO `kelas` VALUES ('XI TC','XI','XI TC',NULL,NULL);
INSERT INTO `kelas` VALUES ('XII NA','XII','XII NA',NULL,NULL);
INSERT INTO `kelas` VALUES ('XII NB','XII','XII NB',NULL,NULL);
INSERT INTO `kelas` VALUES ('XII NC','XII','XII NC',NULL,NULL);
INSERT INTO `kelas` VALUES ('XII ND','XII','XII ND',NULL,NULL);
INSERT INTO `kelas` VALUES ('XII TA','XII','XII TA',NULL,NULL);
INSERT INTO `kelas` VALUES ('XII TB','XII','XII TB',NULL,NULL);
INSERT INTO `kelas` VALUES ('XII TC','XII','XII TC',NULL,NULL);

/*---------------------------------------------------------------
  TABLE: `level`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `level`;
CREATE TABLE `level` (
  `kode_level` varchar(5) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `rombongan_belajar_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kode_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `level` VALUES   ('EX','ALUMNI',NULL);
INSERT INTO `level` VALUES ('X','Kelas X',NULL);
INSERT INTO `level` VALUES ('XI','Kelas XI',NULL);
INSERT INTO `level` VALUES ('XII','Kelas XII',NULL);

/*---------------------------------------------------------------
  TABLE: `log`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_siswa` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `text` varchar(20) NOT NULL,
  `date` varchar(20) NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB AUTO_INCREMENT=33547 DEFAULT CHARSET=latin1;
INSERT INTO `log` VALUES   ('33512','1','logout','keluar','2023-01-29 14:04:03');
INSERT INTO `log` VALUES ('33513','1','logout','keluar','2023-02-05 11:26:24');
INSERT INTO `log` VALUES ('33514','10','logout','keluar','2023-02-12 19:01:57');
INSERT INTO `log` VALUES ('33515','20','logout','keluar','2023-02-12 19:02:31');
INSERT INTO `log` VALUES ('33516','10','logout','keluar','2023-02-12 19:02:52');
INSERT INTO `log` VALUES ('33517','20','logout','keluar','2023-02-12 19:03:27');
INSERT INTO `log` VALUES ('33518','6','logout','keluar','2023-02-12 19:06:11');
INSERT INTO `log` VALUES ('33519','1','logout','keluar','2023-02-12 19:13:06');
INSERT INTO `log` VALUES ('33520','25','logout','keluar','2023-02-12 19:20:56');
INSERT INTO `log` VALUES ('33521','31','logout','keluar','2023-02-12 19:22:09');
INSERT INTO `log` VALUES ('33522','40','logout','keluar','2023-02-12 19:25:28');
INSERT INTO `log` VALUES ('33523','47','logout','keluar','2023-02-12 19:26:56');
INSERT INTO `log` VALUES ('33524','39','logout','keluar','2023-02-12 19:27:17');
INSERT INTO `log` VALUES ('33525','9','logout','keluar','2023-02-12 19:28:26');
INSERT INTO `log` VALUES ('33526','48','logout','keluar','2023-02-12 19:33:47');
INSERT INTO `log` VALUES ('33527','1','logout','keluar','2023-02-12 19:38:17');
INSERT INTO `log` VALUES ('33528','49','logout','keluar','2023-02-12 19:40:58');
INSERT INTO `log` VALUES ('33529','28','logout','keluar','2023-02-12 19:43:54');
INSERT INTO `log` VALUES ('33530','54','logout','keluar','2023-02-12 19:44:13');
INSERT INTO `log` VALUES ('33531','52','logout','keluar','2023-02-12 19:47:44');
INSERT INTO `log` VALUES ('33532','25','logout','keluar','2023-02-12 19:48:20');
INSERT INTO `log` VALUES ('33533','39','logout','keluar','2023-02-12 19:58:41');
INSERT INTO `log` VALUES ('33534','60','logout','keluar','2023-02-12 19:59:19');
INSERT INTO `log` VALUES ('33535','6','logout','keluar','2023-02-13 08:04:38');
INSERT INTO `log` VALUES ('33536','6','logout','keluar','2023-02-13 08:06:13');
INSERT INTO `log` VALUES ('33537','52','logout','keluar','2023-02-13 10:57:30');
INSERT INTO `log` VALUES ('33538','52','logout','keluar','2023-02-13 11:09:09');
INSERT INTO `log` VALUES ('33539','62','logout','keluar','2023-02-13 12:04:02');
INSERT INTO `log` VALUES ('33540','64','logout','keluar','2023-02-13 12:10:32');
INSERT INTO `log` VALUES ('33541','53','logout','keluar','2023-02-13 12:42:58');
INSERT INTO `log` VALUES ('33542','56','logout','keluar','2023-02-14 05:37:50');
INSERT INTO `log` VALUES ('33543','54','logout','keluar','2023-02-14 05:39:30');
INSERT INTO `log` VALUES ('33544','54','logout','keluar','2023-02-14 05:40:20');
INSERT INTO `log` VALUES ('33545','54','logout','keluar','2023-02-14 05:45:03');
INSERT INTO `log` VALUES ('33546','53','logout','keluar','2023-02-14 12:16:09');

/*---------------------------------------------------------------
  TABLE: `mapel`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `mapel`;
CREATE TABLE `mapel` (
  `id_mapel` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) NOT NULL DEFAULT '0',
  `idpk` varchar(255) NOT NULL,
  `idguru` varchar(3) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jml_soal` int(5) NOT NULL,
  `jml_esai` int(5) NOT NULL DEFAULT 0,
  `tampil_pg` int(5) NOT NULL,
  `tampil_esai` int(5) NOT NULL DEFAULT 0,
  `bobot_pg` int(5) NOT NULL,
  `bobot_esai` int(5) NOT NULL DEFAULT 0,
  `level` varchar(5) NOT NULL,
  `opsi` int(1) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` varchar(2) NOT NULL,
  `kkm` int(3) DEFAULT NULL,
  `soal_agama` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_mapel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*---------------------------------------------------------------
  TABLE: `mata_pelajaran`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `mata_pelajaran`;
CREATE TABLE `mata_pelajaran` (
  `kode_mapel` varchar(20) NOT NULL,
  `nama_mapel` varchar(150) NOT NULL,
  `mata_pelajaran_id` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`kode_mapel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `mata_pelajaran` VALUES   ('01','Matematika','MTK');

/*---------------------------------------------------------------
  TABLE: `materi`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `materi`;
CREATE TABLE `materi` (
  `id_materi` int(11) NOT NULL AUTO_INCREMENT,
  `id_guru` int(255) NOT NULL DEFAULT 0,
  `kelas` text NOT NULL,
  `mapel` varchar(255) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `materi` longtext DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `tgl_mulai` datetime NOT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `tgl` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_materi`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
INSERT INTO `materi` VALUES   ('45','329','a:1:{i:0;s:4:\"X NA\";}','01','TestingTe','<p>Testing</p>',NULL,'2022-12-19 22:00:00','','2022-12-19 22:10:27',NULL);
INSERT INTO `materi` VALUES ('46','1','a:1:{i:0;s:4:\"X NA\";}','01','Materi Berhitung 1 - 10','<p>Silahkan klik link dibawah untuk masuk ke soalnya</p><p><a href=\"https://docs.google.com/forms/d/e/1FAIpQLScFLwxlFQkNYyq3WHKjrjo5XyfvYoyvXYwXoPEYsIIEHA4rpQ/viewform?usp=sf_link\" target=\"_blank\">Ujian Modul 1</a><br></p>','1674974798_document.pdf','2023-01-29 13:46:00','','2023-01-29 13:47:18',NULL);
INSERT INTO `materi` VALUES ('48','331','a:1:{i:0;s:4:\"X NA\";}','01','Test','<p>asasasas</p>','1676108470_Hukum Bisnis.docx','2023-02-11 16:37:00','','2023-02-11 16:41:10',NULL);
INSERT INTO `materi` VALUES ('54','332','a:1:{i:0;s:5:\"XI NB\";}','01','Modul 4 Bangkitlah Bangsaku','<p><a href=\"https://emodul.kemdikbud.go.id/B-Pkn-10/mobile/index.html\" target=\"_blank\">https://emodul.kemdikbud.go.id/B-Pkn-10/mobile/index.html</a><br></p>',NULL,'2023-02-12 18:54:00','','2023-02-12 19:03:40',NULL);

/*---------------------------------------------------------------
  TABLE: `materi_komentar`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `materi_komentar`;
CREATE TABLE `materi_komentar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_materi` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `komentar` text DEFAULT NULL,
  `level` varchar(20) DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
INSERT INTO `materi_komentar` VALUES   ('16','50','334','<p>waw bagus banget</p>','guru','2023-02-11 16:45:41');
INSERT INTO `materi_komentar` VALUES ('17','45','28','<p>coba komentar</p>','siswa','2023-02-12 19:19:25');
INSERT INTO `materi_komentar` VALUES ('18','46','28','<p>sudah coba download n jawab ujian&nbsp;</p><p>keren aplikasinya lebih enak disini user interfacenya&nbsp;</p>','siswa','2023-02-12 19:22:28');
INSERT INTO `materi_komentar` VALUES ('19','45','1','','siswa','2023-02-12 19:29:07');
INSERT INTO `materi_komentar` VALUES ('20','45','1','<p>maksud dar isoal nomor 1 itu apa yaa?</p>','siswa','2023-02-12 19:29:23');
INSERT INTO `materi_komentar` VALUES ('21','54','53','<p>Sekarang sudah sampai di modul 10 nggih pak? tapi mohon maaf, tulisan di judulnya \"modul 4 Bangkitlh Bangsaku\"<br></p>','siswa','2023-02-13 10:43:39');
INSERT INTO `materi_komentar` VALUES ('22','46','62','<p>Sukaaa</p>','siswa','2023-02-13 12:02:40');
INSERT INTO `materi_komentar` VALUES ('23','48','62','<p>????????????</p>','siswa','2023-02-13 12:03:16');
INSERT INTO `materi_komentar` VALUES ('24','45','64','<p>Oke banget</p>','siswa','2023-02-13 12:05:06');
INSERT INTO `materi_komentar` VALUES ('25','46','64','<p>????????????</p>','siswa','2023-02-13 12:06:07');
INSERT INTO `materi_komentar` VALUES ('26','54','332','<p>Benar, tulisannya modul 10, namun jika dihitung dari kelas 8 ini masih modul 4,</p><p><br></p>','guru','2023-02-14 10:47:09');
INSERT INTO `materi_komentar` VALUES ('27','54','53','<p>Anak saya masih kelas 7 pak, Kalau untuk materi kelas 7 sekarang sudah modul brp nggih? Untuk kelasnya, kemarin waktu zoom e learning paket B masuk di XI NB. Ini sudah betul kn pak pengelompokan kelasnya? Mohon maaf dan terimakasih sebelumnya <br></p>','siswa','2023-02-14 11:45:06');

/*---------------------------------------------------------------
  TABLE: `meet`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `meet`;
CREATE TABLE `meet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_guru` int(11) DEFAULT NULL,
  `id_mapel` varchar(50) DEFAULT NULL,
  `id_kelas` varchar(11) DEFAULT NULL,
  `room` varchar(100) DEFAULT NULL,
  `judul` varchar(110) DEFAULT NULL,
  `deskripsi` varchar(250) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 0,
  `create_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
INSERT INTO `meet` VALUES   ('10','331','01','X NA','ocbt33101xna','Test Pelatihan ','Test','0','2023-02-12 19:52:35');

/*---------------------------------------------------------------
  TABLE: `nilai`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `nilai`;
CREATE TABLE `nilai` (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_ujian` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `kode_ujian` varchar(30) NOT NULL,
  `ujian_mulai` varchar(20) NOT NULL,
  `ujian_berlangsung` varchar(20) DEFAULT NULL,
  `ujian_selesai` varchar(20) DEFAULT NULL,
  `jml_benar` int(10) DEFAULT NULL,
  `jml_salah` int(10) DEFAULT NULL,
  `nilai_esai` varchar(10) DEFAULT NULL,
  `skor` varchar(10) DEFAULT NULL,
  `total` varchar(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `ipaddress` varchar(20) DEFAULT NULL,
  `hasil` int(2) NOT NULL,
  `jawaban` text DEFAULT NULL,
  `jawaban_esai` longtext DEFAULT NULL,
  `nilai_esai2` text DEFAULT NULL,
  `online` int(1) NOT NULL DEFAULT 0,
  `id_soal` longtext DEFAULT NULL,
  `id_opsi` longtext DEFAULT NULL,
  `id_esai` text DEFAULT NULL,
  `blok` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_nilai`)
) ENGINE=InnoDB AUTO_INCREMENT=11461 DEFAULT CHARSET=latin1;

/*---------------------------------------------------------------
  TABLE: `pengawas`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `pengawas`;
CREATE TABLE `pengawas` (
  `id_pengawas` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `level` varchar(10) DEFAULT NULL,
  `no_ktp` varchar(16) DEFAULT NULL,
  `tempat_lahir` varchar(30) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `agama` varchar(10) DEFAULT NULL,
  `no_hp` varchar(13) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `alamat_jalan` varchar(255) DEFAULT NULL,
  `rt_rw` varchar(8) DEFAULT NULL,
  `dusun` varchar(50) DEFAULT NULL,
  `kelurahan` varchar(50) DEFAULT NULL,
  `kecamatan` varchar(30) DEFAULT NULL,
  `kode_pos` int(6) DEFAULT NULL,
  `nuptk` varchar(20) DEFAULT NULL,
  `bidang_studi` varchar(50) DEFAULT NULL,
  `jenis_ptk` varchar(50) DEFAULT NULL,
  `tgs_tambahan` varchar(50) DEFAULT NULL,
  `status_pegawai` varchar(50) DEFAULT NULL,
  `status_aktif` varchar(20) DEFAULT NULL,
  `status_nikah` varchar(20) DEFAULT NULL,
  `sumber_gaji` varchar(30) DEFAULT NULL,
  `ahli_lab` varchar(10) DEFAULT NULL,
  `nama_ibu` varchar(40) DEFAULT NULL,
  `nama_suami` varchar(50) DEFAULT NULL,
  `nik_suami` varchar(20) DEFAULT NULL,
  `pekerjaan` varchar(20) DEFAULT NULL,
  `tmt` date DEFAULT NULL,
  `keahlian_isyarat` varchar(10) DEFAULT NULL,
  `kewarganegaraan` varchar(10) DEFAULT NULL,
  `npwp` varchar(16) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `ptk_id` varchar(50) DEFAULT NULL,
  `password2` text DEFAULT NULL,
  `ruang` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_pengawas`)
) ENGINE=InnoDB AUTO_INCREMENT=346 DEFAULT CHARSET=utf8;
INSERT INTO `pengawas` VALUES   ('1','-','administrator','','admin','$2y$10$2rTCXEfsEIbKOCKKN5dQKeiJHFh1wlUC208yUKivwJU5rvL0ELGfa','admin','','','0000-00-00','','','','','','','','','','0','','','','','','','','','','','','','','0000-00-00','','','','',NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('331','-','Guru 1',NULL,'guru1','1234567890','guru',NULL,'','2023-02-11','Laki-Laki','','0897665432123',NULL,'Jl. Cakalang 209 A Kel. Polowijen Kec. Blimbing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'-.png',NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('332',' -','ARUL FERY WICAKSONO',NULL,'fery_wicaksono','eevee','guru',NULL,NULL,NULL,'Laki-Laki',NULL,'085648687248',NULL,'KLAMPOK RT.02 RW.02 SINGOSARI MALANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('333',' -','M. RIZKA KHOLIS',NULL,'kholiz','kholiz31','guru',NULL,NULL,NULL,'Laki-Laki',NULL,'08883694993',NULL,'SINGOSARI, MALANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('334',' -','AFIF AULIYA NURANI',NULL,'afifanurani','bismillahi','guru',NULL,'Malang','1997-02-15','perempuan','Islam','083834372008',NULL,'Perumahan Latansa Cluster No. A5 Jalan Tirto Agung',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('335',' -','NOVITA SARI',NULL,'novitasari','novita91','guru',NULL,NULL,NULL,'perempuan',NULL,'08990359424',NULL,'JL.KH.Yusuf No 80 Tasikmadu Lowokwaru Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('336',' -','ANDINI PRIMA NUR&#39;AINI',NULL,'andiniprima','pkbmzamzam','guru',NULL,NULL,NULL,'perempuan',NULL,'085655765985',NULL,'Jl. Tirto Rahayu, gang XI, No 5 C, Landungsari, Dau, Kab. Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('337',' -','RIZQIANA MAULIASARI KURNIAWAN',NULL,'rizqianasari','sari1999','guru',NULL,NULL,NULL,'perempuan',NULL,'085870005475',NULL,'Jl. Terusan Piranha Atas no 95 B, Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('338','-','NADHIRA RIFQI',NULL,'nadhirarifqi','485103','guru',NULL,NULL,NULL,'perempuan',NULL,'089653025309',NULL,'Jl. Teluk Cendrawasih no. 45 D, Arjosari Kecamatan Blimbing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('339',' -','EKY ROZIANA NUGRAHAWATI',NULL,'eky_roziana','ekyroziana','guru',NULL,NULL,NULL,'perempuan',NULL,'08563552348',NULL,'JL. KLAMPOK KRAJAN RT 2 RW 2 DS KLAMPOK SINGOSARI KAB. MALANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('340','-','JONI ISKANDAR',NULL,'joni','inoj','guru',NULL,NULL,NULL,'Laki-Laki',NULL,'088217992900',NULL,'SINGOSARI MALANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('341',' -','KEN SARI SAPUTRI',NULL,'ken sari saputri','kensaricantik','guru',NULL,'Malang','2002-10-11','perempuan','Islam','088217438071',NULL,'JL. MELATI 2 DSN GUNUNGREJO',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('342',' -','RIZQI DWI PURWANTO',NULL,'rizqi','1234567890','guru',NULL,NULL,NULL,'perempuan',NULL,'085158835992',NULL,'JL. SEMBILANG VII NO 82C KEL. POLOWIJEN KEC. BLIMBING',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('343','-','YUSUF NOVAN ALDILA',NULL,'novan','novan811','guru',NULL,'Malang','1994-11-08','Laki-Laki','Islam','083834889221',NULL,'PERUM. PURI KARTIKA ASRI BLOK L-28 TUNJUNGSEKAR MALANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('344',' -','FITRIA IKAPUTRI',NULL,'ikaputri','fitria','guru',NULL,'Malang ','1984-01-06','perempuan','Islam','089621271169',NULL,'JL. KAPI MINDA 1 - 11F NO 5 SAWOJAJAR 2, MALANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `pengawas` VALUES ('345','-','MUCHLIS AGUNG JAYA',NULL,'agung','nindi78','guru',NULL,'Malang aja','2023-02-11','Laki-Laki','Islam','089679107414',NULL,'JL. KAPI MINDA 1-11F NO. 5 SAWOJAJAR 2 MALANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'-.jpg',NULL,NULL,NULL);

/*---------------------------------------------------------------
  TABLE: `pengumuman`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `pengumuman`;
CREATE TABLE `pengumuman` (
  `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `user` int(3) NOT NULL,
  `text` longtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_pengumuman`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
INSERT INTO `pengumuman` VALUES   ('6','eksternal','Pengemuman Pertama','1','<p>1. Pengumuman Pertama</p><p>2. Pengumuman Ketiga</p>','2023-01-29 13:59:54');

/*---------------------------------------------------------------
  TABLE: `pk`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `pk`;
CREATE TABLE `pk` (
  `id_pk` varchar(10) NOT NULL,
  `program_keahlian` varchar(50) NOT NULL,
  `jurusan_id` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `pk` VALUES   ('NAUTIKA','NAUTIKA KAPAL NIAGA',NULL);
INSERT INTO `pk` VALUES ('semua','semua',NULL);
INSERT INTO `pk` VALUES ('TEKNIKA','TEKNIKA KAPAL NIAGA',NULL);

/*---------------------------------------------------------------
  TABLE: `referensi_jurusan`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `referensi_jurusan`;
CREATE TABLE `referensi_jurusan` (
  `jurusan_id` varchar(10) NOT NULL,
  `nama_jurusan` varchar(100) DEFAULT NULL,
  `untuk_sma` int(1) NOT NULL,
  `untuk_smk` int(1) NOT NULL,
  `jenjang_pendidikan_id` int(1) DEFAULT NULL,
  PRIMARY KEY (`jurusan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*---------------------------------------------------------------
  TABLE: `registrasi_siswa`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `registrasi_siswa`;
CREATE TABLE `registrasi_siswa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nis` varchar(30) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `id_kelas` varchar(11) DEFAULT NULL,
  `alamat` varchar(250) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(30) DEFAULT NULL,
  `agama` varchar(10) DEFAULT NULL,
  `nama_ayah` varchar(150) DEFAULT NULL,
  `nama_ibu` varchar(150) DEFAULT NULL,
  `th_masuk` int(5) DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `username_login` varchar(50) DEFAULT NULL,
  `password_login` varchar(255) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `registrasi_nis_siswa` (`nis`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;
INSERT INTO `registrasi_siswa` VALUES   ('101','18010093','Qalesya Lintang Aina','X NA','Alamat penerima   :\r\n*Rumah Lintang*\r\nJl Perusahaan gg.3 rt/rw 03/08 losawi, kelurahan Tunjungtirto, kec singosari (Depan kampus ITN 2)\r\nSingosari -  Malang\r\n????Ancer2 ????\r\nGang sebelah toko Pak jalil *TIKI* masuk, ada 2x perempatan lurus aja ,\r\nKe','Malang ','2011-09-28','Perempuan','Islam','Elin W','Khaninul afidah','2023','089680661005','18010093','18010093','2023-02-13 17:38:23');
INSERT INTO `registrasi_siswa` VALUES ('102','20010187','Abdurrohman Sayyid Kelana Sasmitaning Manah','X NA','Jl. Mojowarno No.35 RT.20 RW.03 Kajang Mojorejo Junrejo Kota Batu Jawa Timur 65322','Kediri','2013-12-05','Laki-Laki','Islam','Eliya Omayanti','Eliya Omayanti','2020','0881026405258','20010187','20010187','2023-02-14 08:23:51');
INSERT INTO `registrasi_siswa` VALUES ('103','22010293','Hilarius Dominico Rahandika Yoga Subiantoro','X NA','Graha Sejahtera Residence H.1 Kandungan, Landungsari, Dau, Kabupaten Malang ','Malang','2015-09-15','Laki-Laki','Kristen Ka','Yohanes Subiantoro','Fransiska Uut','2022','085106091190','22010293','22010293','2023-02-14 09:02:26');
INSERT INTO `registrasi_siswa` VALUES ('104','20030352','DIYYAH  DEWI ROBI\'AH','XII NC','jl. h. alwi rt. 04 rw. 10 pulesari tirtomoyo pakis','malang','2004-09-29','Perempuan','Islam','sudjiono','lik anah','2020','(+62) 814500448','20030352','20030352','2023-02-14 17:43:24');

/*---------------------------------------------------------------
  TABLE: `ruang`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `ruang`;
CREATE TABLE `ruang` (
  `kode_ruang` varchar(10) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  PRIMARY KEY (`kode_ruang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `ruang` VALUES   ('R1','ROOM DARING 1');
INSERT INTO `ruang` VALUES ('R2','ROOM DARING 2');
INSERT INTO `ruang` VALUES ('R3','ROOM DARING 3');

/*---------------------------------------------------------------
  TABLE: `savsoft_options`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `savsoft_options`;
CREATE TABLE `savsoft_options` (
  `oid` int(11) NOT NULL AUTO_INCREMENT,
  `qid` int(11) NOT NULL,
  `q_option` text NOT NULL,
  `q_option_match` varchar(1000) DEFAULT NULL,
  `score` float NOT NULL DEFAULT 0,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*---------------------------------------------------------------
  TABLE: `savsoft_qbank`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `savsoft_qbank`;
CREATE TABLE `savsoft_qbank` (
  `qid` int(11) NOT NULL AUTO_INCREMENT,
  `question_type` varchar(100) NOT NULL DEFAULT 'Multiple Choice Single Answer',
  `question` text NOT NULL,
  `description` text NOT NULL,
  `cid` int(11) NOT NULL,
  `lid` int(11) NOT NULL,
  `no_time_served` int(11) NOT NULL DEFAULT 0,
  `no_time_corrected` int(11) NOT NULL DEFAULT 0,
  `no_time_incorrected` int(11) NOT NULL DEFAULT 0,
  `no_time_unattempted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`qid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*---------------------------------------------------------------
  TABLE: `semester`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `semester`;
CREATE TABLE `semester` (
  `semester_id` varchar(5) NOT NULL,
  `tahun_ajaran_id` varchar(4) NOT NULL,
  `nama_semester` varchar(50) NOT NULL,
  `semester` int(1) NOT NULL,
  `periode_aktif` enum('1','0') NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  PRIMARY KEY (`semester_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*---------------------------------------------------------------
  TABLE: `server`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `server`;
CREATE TABLE `server` (
  `kode_server` varchar(20) NOT NULL,
  `nama_server` varchar(30) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*---------------------------------------------------------------
  TABLE: `sesi`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `sesi`;
CREATE TABLE `sesi` (
  `kode_sesi` varchar(10) NOT NULL,
  `nama_sesi` varchar(30) NOT NULL,
  PRIMARY KEY (`kode_sesi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `sesi` VALUES   ('1','Sesi Pagi');
INSERT INTO `sesi` VALUES ('2','Sesi Siang');
INSERT INTO `sesi` VALUES ('3','Sesi Sore');

/*---------------------------------------------------------------
  TABLE: `session`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_time` varchar(10) NOT NULL,
  `session_hash` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*---------------------------------------------------------------
  TABLE: `setting`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `id_setting` int(11) NOT NULL AUTO_INCREMENT,
  `aplikasi` varchar(100) DEFAULT NULL,
  `kode_sekolah` varchar(10) DEFAULT NULL,
  `sekolah` varchar(50) DEFAULT NULL,
  `jenjang` varchar(5) DEFAULT NULL,
  `kepsek` varchar(50) DEFAULT NULL,
  `nip` varchar(30) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `kecamatan` varchar(50) DEFAULT NULL,
  `kota` varchar(30) DEFAULT NULL,
  `telp` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `web` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `logo` text DEFAULT NULL,
  `header` text DEFAULT NULL,
  `header_kartu` text DEFAULT NULL,
  `nama_ujian` text DEFAULT NULL,
  `versi` varchar(10) DEFAULT NULL,
  `ip_server` varchar(100) DEFAULT NULL,
  `waktu` varchar(50) DEFAULT NULL,
  `server` varchar(50) DEFAULT NULL,
  `id_server` varchar(50) DEFAULT NULL,
  `url_host` varchar(50) DEFAULT NULL,
  `token_api` varchar(50) DEFAULT NULL,
  `sekolah_id` varchar(50) DEFAULT NULL,
  `npsn` varchar(10) DEFAULT NULL,
  `db_versi` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_setting`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
INSERT INTO `setting` VALUES   ('1','E-LEARNING PKBM ZAMZAM','P2961743','PKBM ZAMZAM','SD','Dian Mukti Wibowo','-','Jl. Cakalang 209 A Kel. Polowijen','Blimbing ','Malang','','','','','dist/img/logo87.png','','KARTU PESERTA','Penilaian Tengah Semester','2.5','https://pkbm.zamzam.sch.id/elearning','Asia/Jakarta','lokal','SR01','https://pkbm.zamzam.sch.id','LBV8rj1kmht3oW','8cce47df-aae7-4274-83cb-5af3093eab56','69787351','2.8.1');

/*---------------------------------------------------------------
  TABLE: `sinkron`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `sinkron`;
CREATE TABLE `sinkron` (
  `nama_data` varchar(50) NOT NULL,
  `data` varchar(50) DEFAULT NULL,
  `jumlah` varchar(50) DEFAULT NULL,
  `tanggal` varchar(50) DEFAULT NULL,
  `status_sinkron` int(11) DEFAULT NULL,
  PRIMARY KEY (`nama_data`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `sinkron` VALUES   ('DATA1','siswa','','','0');
INSERT INTO `sinkron` VALUES ('DATA2','bank soal','','','0');
INSERT INTO `sinkron` VALUES ('DATA3','soal','','','0');
INSERT INTO `sinkron` VALUES ('DATA4','jadwal','','','0');

/*---------------------------------------------------------------
  TABLE: `siswa`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `siswa`;
CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL AUTO_INCREMENT,
  `id_kelas` varchar(11) DEFAULT NULL,
  `idpk` varchar(10) DEFAULT NULL,
  `nis` varchar(30) DEFAULT NULL,
  `no_peserta` varchar(30) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `level` varchar(5) DEFAULT NULL,
  `ruang` varchar(10) DEFAULT NULL,
  `sesi` int(2) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `server` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(30) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `agama` varchar(10) DEFAULT NULL,
  `kebutuhan_khusus` varchar(20) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `rt` varchar(5) DEFAULT NULL,
  `rw` varchar(5) DEFAULT NULL,
  `dusun` varchar(100) DEFAULT NULL,
  `kelurahan` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(100) DEFAULT NULL,
  `kode_pos` int(10) DEFAULT NULL,
  `jenis_tinggal` varchar(100) DEFAULT NULL,
  `alat_transportasi` varchar(100) DEFAULT NULL,
  `hp` varchar(15) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `skhun` int(11) DEFAULT NULL,
  `no_kps` varchar(50) DEFAULT NULL,
  `nama_ayah` varchar(150) DEFAULT NULL,
  `tahun_lahir_ayah` int(4) DEFAULT NULL,
  `pendidikan_ayah` varchar(50) DEFAULT NULL,
  `pekerjaan_ayah` varchar(100) DEFAULT NULL,
  `penghasilan_ayah` varchar(100) DEFAULT NULL,
  `nohp_ayah` varchar(15) DEFAULT NULL,
  `nama_ibu` varchar(150) DEFAULT NULL,
  `tahun_lahir_ibu` int(4) DEFAULT NULL,
  `pendidikan_ibu` varchar(50) DEFAULT NULL,
  `pekerjaan_ibu` varchar(100) DEFAULT NULL,
  `penghasilan_ibu` varchar(100) DEFAULT NULL,
  `nohp_ibu` int(15) DEFAULT NULL,
  `nama_wali` varchar(150) DEFAULT NULL,
  `tahun_lahir_wali` int(4) DEFAULT NULL,
  `pendidikan_wali` varchar(50) DEFAULT NULL,
  `pekerjaan_wali` varchar(100) DEFAULT NULL,
  `penghasilan_wali` varchar(50) DEFAULT NULL,
  `angkatan` int(5) DEFAULT NULL,
  `nisn` varchar(50) DEFAULT NULL,
  `peserta_didik_id` varchar(50) DEFAULT NULL,
  `semester_id` varchar(10) DEFAULT NULL,
  `rombongan_belajar_id` varchar(50) DEFAULT NULL,
  `status` int(1) DEFAULT 1,
  `online` int(1) DEFAULT 0,
  PRIMARY KEY (`id_siswa`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
INSERT INTO `siswa` VALUES   ('1','X NA',NULL,'0987','0987','Rizqi Dwi','X',NULL,NULL,'0987','0987',NULL,NULL,'Laki-Laki','Malang','2000-01-01','Islam',NULL,'Jl. Cakalang 209 A Kel. Polowijen Kec. Blimbing 65126 Kota Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234567890',NULL,NULL,NULL,'Bapak',NULL,NULL,NULL,NULL,NULL,'Ibuk',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('2','X NA',NULL,'19010114','19010114','NAJWA EGI ANAMIKA','X',NULL,NULL,'19010114','19010114',NULL,NULL,'Perempuan','MALANG','2012-10-20','Islam',NULL,'JL. KAPI MINDA I - 11F NO. 5 SAWOJAJAR 2, MALANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081938982394',NULL,NULL,NULL,'AGUNG',NULL,NULL,NULL,NULL,NULL,'IKA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('3','X NA',NULL,'456789','456789','Rizqi Dwi Purwanto','X',NULL,NULL,'456789','456789',NULL,NULL,'Laki-Laki','Malang','2007-01-01','Islam',NULL,'Jl. Sembilang VII No.82C Kel Polowijen, Kec. Blimbing Kota Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'08585158835992',NULL,NULL,NULL,'Nama Ayah',NULL,NULL,NULL,NULL,NULL,'Nama Ibu kandung',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('4','X NA',NULL,'18010076','18010076','Almyra Ranaa Rabbani ','X',NULL,NULL,'18010076','18010076',NULL,NULL,'Perempuan','Pasuruan ','2011-08-05','Islam',NULL,'Jl. Tenis meja no.57B Tasikmadu kel. Lowokwaru kota Malang 65143',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081259605352',NULL,NULL,NULL,'Slamet Hariadi',NULL,NULL,NULL,NULL,NULL,'Vita swastika',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('5','X NA',NULL,'19010115','19010115','Muhammad Zaidan Zaky ','X',NULL,NULL,'19010115','19010115',NULL,NULL,'Laki-Laki','Malang ','2011-12-23','Islam',NULL,'Perumahan permata land no A57 jalan Akordion Utara Tunggul Wulung ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081216005148',NULL,NULL,NULL,'Mohammad Eko Jeenury',NULL,NULL,NULL,NULL,NULL,'Firdia Eka Putri ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('6','X NA',NULL,'19010135','19010135','Ahmad Arfan Aryoseto','X',NULL,NULL,'19010135','19010135',NULL,NULL,'Laki-Laki','Jakarta','2012-08-29','Islam',NULL,'Jl. Dirgantara II blok CIII no. 13, Kel. Lesanpuro, Kec. Kedungkandang, Kota Malang 65138',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'087808783081',NULL,NULL,NULL,'Aresto Yudo Sujono',NULL,NULL,NULL,NULL,NULL,'Mirta Amalia',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('7','X NA',NULL,'20010143','20010143','Queen Amirah Az-Zahrah','X',NULL,NULL,'20010143','20010143',NULL,NULL,'Perempuan','Malang','2012-06-04','Islam',NULL,'Jl. Industri Gg. 4 RT. 06 RW. 03, Mendit Barat. Malang Jatim',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081233444940',NULL,NULL,NULL,'Sutikno',NULL,NULL,NULL,NULL,NULL,'Mulyani',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('8','X NA',NULL,'21010268','21010268','John Phillip Caldas','X',NULL,NULL,'21010268','21010268',NULL,NULL,'Laki-Laki','Kediri ','2015-09-09','Kristen Pr',NULL,'Jl. Mbah Singo dusun kandangan desa gunung sari kec. Bumiaji Kota Batu Jawa Timur.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'085258140681',NULL,NULL,NULL,'Fabio Monteiro Caldas',NULL,NULL,NULL,NULL,NULL,'Ester Mei Aryani',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('9','X NA',NULL,'20010149','20010149','Reinoa Qaireen Almaira Darmanto','X',NULL,NULL,'20010149','20010149',NULL,NULL,'Perempuan','Mojokerto','2013-12-28','Islam',NULL,'Jl. Danau Maninjau Dalam 1, Blok B2A3\r\nKel. Sawojajar, Kec. Kedungkandang, Kota Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'085855127821',NULL,NULL,NULL,'Hendy Kusdarmanto',NULL,NULL,NULL,NULL,NULL,'Sri Wahyuni Niningsih',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('10','XII NC',NULL,'20020208','20020208','Muhammad Aurat hibban','XII',NULL,NULL,'20020208','20020208',NULL,NULL,'Laki-Laki','Tangerang','2007-03-03','Islam',NULL,'Desa ngenep utara rt 01 rw03',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'082213025867',NULL,NULL,NULL,'Prasetia Wardhana',NULL,NULL,NULL,NULL,NULL,'Melly Meildayani',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('11','XI NB',NULL,'20020221','20020221','Ilham Muhammad Fatin ','XI ',NULL,NULL,'20020221','20020221',NULL,NULL,'Laki-Laki','Malang ','2006-08-31','Islam',NULL,'Jl. candi mendut v no 9 kel Mojolangu kec Lowokwaru ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081233297629',NULL,NULL,NULL,'Catur Mulyono ',NULL,NULL,NULL,NULL,NULL,'Martina Sylviarini ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('12','X NA',NULL,'21010217','21010217','Hafshah An Nasyamah','X',NULL,NULL,'21010217','21010217',NULL,NULL,'Perempuan','Malang','2014-12-01','Islam',NULL,'Jl. Jayasrani 1 7H/3, Sekarpuro, Pakis, Kab. Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'085232805292',NULL,NULL,NULL,'Firmansyah Ayatullah',NULL,NULL,NULL,NULL,NULL,'Sandra Reri Nufrida',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('13','X NA',NULL,'21010228','21010228','RAJ Maryam Muthmainnah','X',NULL,NULL,'21010228','21010228',NULL,NULL,'Perempuan','Malang','2014-06-28','Islam',NULL,'Perumahan Mutiara Jingga Kav 28 A. Tunggulwulung Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081252650806',NULL,NULL,NULL,'RB. Abd Raoefurrachim',NULL,NULL,NULL,NULL,NULL,'Rindang Rihadini',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('14','X NA',NULL,'21010230','21010230','Januzaj Ruwaifi','X',NULL,NULL,'21010230','21010230',NULL,NULL,'Laki-Laki','Surabaya','2014-05-21','Islam',NULL,'Plaosan Timur no 25 RT 5 RW 9 BLIMBING MALANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'085755757955',NULL,NULL,NULL,'Indra Susilo',NULL,NULL,NULL,NULL,NULL,'Inayah Fajarwati',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('15','X NA',NULL,'21010231','21010231','Dastan Raghuf Khairan','X',NULL,NULL,'21010231','21010231',NULL,NULL,'Laki-Laki','Surabaya','2013-01-20','Islam',NULL,'Jl.Plaosan Timur no 25 RT 5 RW 9 PURWODADI BLIMBING MALANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'082139718971',NULL,NULL,NULL,'Indra Susilo',NULL,NULL,NULL,NULL,NULL,'Inayah Fajarwati',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('16','XII NC',NULL,'22030377','22030377','Ashfa Arina Al Haq','XII',NULL,NULL,'22030377','22030377',NULL,NULL,'Perempuan','Malang','2006-02-15','Islam',NULL,'Perumahan Pesona Singosari Blok jj-6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'082131530928',NULL,NULL,NULL,'Hari Eko Yuliono',NULL,NULL,NULL,NULL,NULL,'Fifin Damayanti',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('17','X NA',NULL,'19010118','19010118','Akbar Putra Adila','X',NULL,NULL,'19010118','19010118',NULL,NULL,'Laki-Laki','Surabaya','2013-02-05','Islam',NULL,'Galaxi bumi permai N3 no. 9 Surabaya',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081331658998',NULL,NULL,NULL,'Andi Sufariyanto',NULL,NULL,NULL,NULL,NULL,'Laila',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('18','X NA',NULL,'19010112','19010112','Nayla Nararya Wijaya','X',NULL,NULL,'19010112','19010112',NULL,NULL,'Perempuan','Malang','2012-04-04','Islam',NULL,'Jl. Sidomakmur IV G12 Kalianyar, Sidodadi, Lawang, Kab. Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081334720258',NULL,NULL,NULL,'Yohan Vindya Wijaya',NULL,NULL,NULL,NULL,NULL,'Herma Indah Palupi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('19','X NA',NULL,'18010100','18010100','Rahima Haura Tasnim Batsnah','X',NULL,NULL,'18010100','18010100',NULL,NULL,'Perempuan','Malang','2011-02-24','Islam',NULL,'Jl Danau Sentani Timur I H1 B2 Kota Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081944910049',NULL,NULL,NULL,'Mohamad Syaerofi Ansori',NULL,NULL,NULL,NULL,NULL,'Margaretha Teodora',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('20','XII NC',NULL,'20020209','20020209','Muhammad Naufal Rabbani','XII',NULL,NULL,'20020209','20020209',NULL,NULL,'Laki-Laki','Bogor','2005-12-26','Islam',NULL,'Ngenep utara rt 01 rw 03',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081259752889',NULL,NULL,NULL,'Prasetia Wardhana',NULL,NULL,NULL,NULL,NULL,'Melly Meildayani',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('21','X NA',NULL,'21010259','21010259','Muhammad Krishna Alkahfi Putra Wijaya','X',NULL,NULL,'21010259','21010259',NULL,NULL,'Laki-Laki','Malang','2013-11-22','Islam',NULL,'Petungroto RT 06/RW 01 Babadan, Ngajum - Malang ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'08563544862',NULL,NULL,NULL,'Alix Wijaya Hadi Kusuma ',NULL,NULL,NULL,NULL,NULL,'Suwarni Fidiansyah Wahab',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('22','XI NA',NULL,'21010251','21010251','Abiyy Rabbani Putra Rahman','XI',NULL,NULL,'21010251','21010251',NULL,NULL,'Laki-Laki','Malang','2010-01-03','Islam',NULL,'Jl.a.yani no.261 lawang - malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081555657585',NULL,NULL,NULL,'Arief Rahman',NULL,NULL,NULL,NULL,NULL,'Faizah',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('23','XI NB',NULL,'22020251','22020251','NURUL ANNISA FITRI','XI ',NULL,NULL,'22020251','22020251',NULL,NULL,'Perempuan','SITUBONDO','2008-10-02','Islam',NULL,'PERUM DE VALLEY BLOK E 12 A',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'085655526111',NULL,NULL,NULL,'SUHARIYANTO',NULL,NULL,NULL,NULL,NULL,'ASYAMI',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('24','X NA',NULL,'21010267','21010267','Abqariah Khaulah Arifin','X',NULL,NULL,'21010267','21010267',NULL,NULL,'Perempuan','Malang','2015-07-18','Islam',NULL,'Jl. Kapi Woro II no. 15 rt. 2/12 Mangliawan malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'083833277058',NULL,NULL,NULL,'Samsul Arifin',NULL,NULL,NULL,NULL,NULL,'Novi Silviana Rizky',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('25','X NA',NULL,'22010341','22010341','Ashiilah Azzahra ','X',NULL,NULL,'22010341','22010341',NULL,NULL,'Perempuan','Pangkalpinang ','2012-09-11','Islam',NULL,'Jl muharto 7 gang bunga RT 04 RW 10 no 55 kelurahan kotalama kecamatan kedungkandang kota malang ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'082371353091',NULL,NULL,NULL,'Hasbullah ',NULL,NULL,NULL,NULL,NULL,'Veni kurniawati ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('26','X NA',NULL,'21010269','21010269','Ahmad idhoh rusydan','X',NULL,NULL,'21010269','21010269',NULL,NULL,'Laki-Laki','Malang','2014-07-21','Islam',NULL,'Kumis kucing dalam no.5 RT.3 RW.2 Lowokwaru malang\r\nLowokwaru',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081805063700',NULL,NULL,NULL,'Nuril asyhuri',NULL,NULL,NULL,NULL,NULL,'Masrifatul hanim',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('27','X NA',NULL,'22010275','22010275','HAGIA SOPHIA RYU','X',NULL,NULL,'22010275','22010275',NULL,NULL,'Perempuan','Gresik ','2015-08-02','Islam',NULL,'Perum Griya Bungah Asri Blok N5 Bungah Gresik',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'087862014606',NULL,NULL,NULL,'HUSNUK FITHON',NULL,NULL,NULL,NULL,NULL,'JUNDA YANTI',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('28','X NA',NULL,'22010280','22010280','Bonaventura Bastonio Adikara Do Berdika','X',NULL,NULL,'22010280','22010280',NULL,NULL,'Laki-Laki','Surabaya','2015-04-15','Kristen Ka',NULL,'Graha Kartika Sulfat \r\njl. Simpang Sulfat Utara VII Blok G-37',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081703303908',NULL,NULL,NULL,'Bernadi Waskito',NULL,NULL,NULL,NULL,NULL,'Kartika Cahaya W',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('29','X NA',NULL,'21010227','21010227','Aila Puan Putri Fibriani','X',NULL,NULL,'21010227','21010227',NULL,NULL,'Perempuan','Banyuwangi','2014-02-27','Islam',NULL,'Jl bandara narita blok ed 4 omaview cemorokandang, kedungkandang, malang, jatim',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081336725381',NULL,NULL,NULL,'Dwie cahyo lukitohadi utomo',NULL,NULL,NULL,NULL,NULL,'Dewi kurnia sari',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('30','X NA',NULL,'22010282','22010282','Gabriella Argwen Arifianto','X',NULL,NULL,'22010282','22010282',NULL,NULL,'Perempuan','Malang','2015-09-10','Kristen Ka',NULL,'Jl LA Sucipto Gg 7B No 3 Rt 7 Rw 2 Kel Blimbing Kec Blimbing Kota Malang 65125',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081350724022',NULL,NULL,NULL,'Gustaf Teguh Arifianto',NULL,NULL,NULL,NULL,NULL,'Brigida Rahendaruri Dwi Putri',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('31','X NA',NULL,'22010288','22010288','Faiz Putra Adila','X',NULL,NULL,'22010288','22010288',NULL,NULL,'Laki-Laki','Surabaya','2015-10-10','Islam',NULL,'Galaxi bumi permai N3 no. 9 Surabaya',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081331658998',NULL,NULL,NULL,'Andi Sufariyanto',NULL,NULL,NULL,NULL,NULL,'Laila',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('32','XI NB',NULL,'22020246','22020246','Shafira Nabila Ruswandya','XI ',NULL,NULL,'22020246','22020246',NULL,NULL,'Perempuan','Malang','2008-07-01','Islam',NULL,'Jl. Selorejo No. 59 Kel. Lowokwaru Kec. Lowokwaru Kota Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081330952875',NULL,NULL,NULL,'Rusmawantono',NULL,NULL,NULL,NULL,NULL,'Nur Widya Lestari',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('33','XI NB',NULL,'20020204','20020204','Erlangga Satria Widodo ','XI ',NULL,NULL,'20020204','20020204',NULL,NULL,'Laki-Laki','Nganjuk ','2007-08-01','Islam',NULL,'Perum Pondok Wisata Estate Blok H nomor 2\r\nJl. Raya Abdulrahman Saleh \r\nMalang ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081230529615',NULL,NULL,NULL,'Sugeng Widodo ',NULL,NULL,NULL,NULL,NULL,'Rizki Widiastuti ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('34','X NA',NULL,'22010273','22010273','Lentera Luika Manik','X',NULL,NULL,'22010273','22010273',NULL,NULL,'Laki-Laki','Surabaya','2013-10-16','Kristen Pr',NULL,'Valencia Residence AA6 No. 61, Gemurung, Gedangan.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081230370100',NULL,NULL,NULL,'Luhu',NULL,NULL,NULL,NULL,NULL,'Okta',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('35','X NA',NULL,'21010224','21010224','Nakhla Fathan Elrafif','X',NULL,NULL,'21010224','21010224',NULL,NULL,'Laki-Laki','Malang','2014-12-30','Islam',NULL,'Perum Harmony Place A20, Saptorenggo, Pakis',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'082337792275',NULL,NULL,NULL,'Hasan Amrullah',NULL,NULL,NULL,NULL,NULL,'Dwi Wirastianti Novita Sari',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('36','X NA',NULL,'21010265','21010265','Sabrina Sarah Ratri Laxita ','X',NULL,NULL,'21010265','21010265',NULL,NULL,'Perempuan','Malang ','2014-07-07','Islam',NULL,'Jl. Guntur No.3 Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'08990303103 ',NULL,NULL,NULL,'Raden Laxita Tri Jogatama ',NULL,NULL,NULL,NULL,NULL,'Yekti Dina Wahyuningrum',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('37','XI NB',NULL,'22020237','22020237','Alkhawarizmi Rasyad Rabbani','XI ',NULL,NULL,'22020237','22020237',NULL,NULL,'Laki-Laki','Pasuruan','2008-11-05','Islam',NULL,'Jln. Tenis Meja No. 57B, Tasikmadu, Kecamatan Lowokwaru, Kota Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'085932163038',NULL,NULL,NULL,'Slamet Hariadi',NULL,NULL,NULL,NULL,NULL,'Vita Swastika',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('38','X NA',NULL,'21010225','21010225','Muhammad Shalahuddin Al Ayyubi','X',NULL,NULL,'21010225','21010225',NULL,NULL,'Laki-Laki','Malang','2014-05-29','Islam',NULL,'Jln. Sulfat agung III no.18, kelurahan Purwantoro, kecamatan Blimbing, Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'085234020594',NULL,NULL,NULL,'Naim Muhdori',NULL,NULL,NULL,NULL,NULL,'Galuh Andina Putri',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('39','X NA',NULL,'19010132','19010132','Adeodatus Louis Maria Carmenito','X',NULL,NULL,'19010132','19010132',NULL,NULL,'Laki-Laki','Malang','2012-09-17','Kristen Ka',NULL,'Perum Griya Sejahtera LPK III, Jl. Joko Kendil B3-16\r\nPandanlandung, Wagir, Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'082330906379',NULL,NULL,NULL,'Sugiyo Pranoto',NULL,NULL,NULL,NULL,NULL,'Maria Kristanti Adi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('40','X NA',NULL,'21010234','21010234','Annelies Kinaya Haromain ','X',NULL,NULL,'21010234','21010234',NULL,NULL,'Perempuan','Situbondo','2015-01-15','Islam',NULL,'Jl Wiryorejo RT/RW 02/06 No 2 (Samping Mesjid Al Amin), Dadaptulis Dalam, Kel Dadaprejo, Kec Junrejo, Kota Batu',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081255578850',NULL,NULL,NULL,'Abdulloh Ansori',NULL,NULL,NULL,NULL,NULL,'Wiwik Wahyuni ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('41','X NA',NULL,'22010340','22010340','Fania Syifa Almira','X',NULL,NULL,'22010340','22010340',NULL,NULL,'Perempuan','Malang','2012-09-01','Islam',NULL,'Jl. Ngantang 43, Kecamatan Lowokwaru, Kelurahan Lowokwaru, Kota Malang, Kode Pos 65141',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'089699884554',NULL,NULL,NULL,'Priyo Utomo',NULL,NULL,NULL,NULL,NULL,'Nur Indria P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('42','XI NB',NULL,'22020242','22020242','Ghiyats Dhiyaullah Ramadhan','XI ',NULL,NULL,'22020242','22020242',NULL,NULL,'Laki-Laki','Tuban','2008-09-01','Islam',NULL,'Jalan karang ampel timur no 13 Ndoro, karang widoro kec Dau Kab. Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081335302642',NULL,NULL,NULL,'Purwo nugroho',NULL,NULL,NULL,NULL,NULL,'Miftakhin nur aviatin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('43','X NA',NULL,'21010222','21010222','ASMANINA ABRARABBANI','X',NULL,NULL,'21010222','21010222',NULL,NULL,'Perempuan','JOMBANG','2014-08-19','Islam',NULL,'PERUMAHAN ANDALUSIA REGENCY BLOK D NO 14. \r\nKECAMATAN SIDOMUKTI\r\nKABUPATEN GRESIK\r\nJAWATIMUR\r\nKODE POS 61121',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'085652050260 / ',NULL,NULL,NULL,'ANDIK NUR ACHMAD',NULL,NULL,NULL,NULL,NULL,'ITSNITA HUSNUFARDANI',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('44','XI NB',NULL,'1829','1829','Rizqi Dwi Purwanto','XI ',NULL,NULL,'1829','1829',NULL,NULL,'Laki-Laki','Malang','2018-01-01','Islam',NULL,'Jl. Sembilang VII No.82C Kel Polowijen Kec Blimbing Kota Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'08585158835992',NULL,NULL,NULL,'Nama Ayah',NULL,NULL,NULL,NULL,NULL,'Nama Ibu kandung',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('45','X NA',NULL,'19010110','19010110','Azka Hayza Al Hakim','X',NULL,NULL,'19010110','19010110',NULL,NULL,'Laki-Laki','Malang','2012-08-13','Islam',NULL,'Perum GPA Ngijo blok IJ-15, Karangploso',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0817534558',NULL,NULL,NULL,'Sandhi Sasmito',NULL,NULL,NULL,NULL,NULL,'Inayatul Laili',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('46','XI NA',NULL,'21010251','21010251','Abiyy Rabbani Putra Rahman','XI',NULL,NULL,'21010251','21010251',NULL,NULL,'Laki-Laki','Malang','2010-01-03','Islam',NULL,'Jl.A.yani no.261 desa :sumberporong kecamatan :lawang kabupaten :Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081555657585',NULL,NULL,NULL,'Arief Rahman',NULL,NULL,NULL,NULL,NULL,'Faizah',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('47','X NA',NULL,'22010292','22010292','Yuan Kineta Quinnova ','X',NULL,NULL,'22010292','22010292',NULL,NULL,'Perempuan','Situbondo','2016-04-20','Islam',NULL,'Jl Wiryorejo RT/RW 02/06 No 2 (Samping Mesjid Al Amin), Dadaptulis Dalam, Kel Dadaprejo, Kec Junrejo, Kota Batu',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081255578850',NULL,NULL,NULL,'Abdulloh Ansori ',NULL,NULL,NULL,NULL,NULL,'Wiwik Wahyuni ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('48','X NA',NULL,'22010342','22010342','Abdullah Rafif ','X',NULL,NULL,'22010342','22010342',NULL,NULL,'Laki-Laki','Malang','2014-02-02','Islam',NULL,'Jl muharto 7 gang bunga RT 04 RW 10 no 55 kelurahan kotalama kecamatan kedungkandang kota malang ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'082371353091',NULL,NULL,NULL,'Hasbullah ',NULL,NULL,NULL,NULL,NULL,'Veni kurniawati ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('49','XI NB',NULL,'22020236','22020236','Graciela Maria Carmenita','XI ',NULL,NULL,'22020236','22020236',NULL,NULL,'Perempuan','Malang','2009-05-10','Kristen Ka',NULL,'Perum Griya Sejahtera LPK III, Jl. Joko Kendil B3-16\r\nPandanlandung, Wagir, Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'082330906379',NULL,NULL,NULL,'Sugiyo Pranoto',NULL,NULL,NULL,NULL,NULL,'Maria Kristanti Adi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('50','X NA',NULL,'21010221','21010221','Qanita Ulya Ghaitsan','X',NULL,NULL,'21010221','21010221',NULL,NULL,'Perempuan','Batu','2015-01-23','Islam',NULL,'Jl. Wukir Gg. I No. 16 Kel. Temas, Kota Batu, Jawa Timur',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'082230928282',NULL,NULL,NULL,'Farisul Islami',NULL,NULL,NULL,NULL,NULL,'Dian Retno Ayuning Tyas',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('51','X NA',NULL,'19010128','19010128','Diastava Aeon Lexmana ','X',NULL,NULL,'19010128','19010128',NULL,NULL,'Laki-Laki','Malang','2012-07-30','Islam',NULL,'Perum Graha Asri Sukodono, Jl. Nanas blok AP No. 3, RT 35 RW 10 Pekarungan - Sukodono. Sidoarjo 61258.\r\n',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081555666148',NULL,NULL,NULL,'Bram Lexmana Putra',NULL,NULL,NULL,NULL,NULL,'Diana Mila Sari ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('52','X NA',NULL,'21010220','21010220','Stevia Veda Mukti Wijaya ','X',NULL,NULL,'21010220','21010220',NULL,NULL,'Perempuan','Malang','2016-07-18','Islam',NULL,'Jl. Gembrung no. 111\r\nBakungan, Glagah\r\nBanyuwangi ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'085259323332',NULL,NULL,NULL,'Ady Wahyu Wijaya ',NULL,NULL,NULL,NULL,NULL,'Niken Panggayuh Mukti ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('53','XI NB',NULL,'22020253','22020253','Kirana Nathania Putri','XI ',NULL,NULL,'22020253','22020253',NULL,NULL,'Perempuan','Gresik','2007-06-17','Islam',NULL,'Komplek Graha Akordion Blok C no 2\r\nKel.Tunggulwulung, Kec. Lowokwaru, Malang Kota',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081249651465',NULL,NULL,NULL,'Ri Aat Yunan Basori',NULL,NULL,NULL,NULL,NULL,'Mamiek Supatmi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2023',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('54','X NA',NULL,'19010107','19010107','Ahmad Nashri','X',NULL,NULL,'19010107','19010107',NULL,NULL,'Laki-Laki','Malang','2009-11-14','Islam',NULL,'Jl. Ki Ageng Gribig Gang 5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081555651365',NULL,NULL,NULL,'Yudie Wiro',NULL,NULL,NULL,NULL,NULL,'Dina Aulia',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('55','X NA',NULL,'22010283','22010283','Muhammad Bilal Ramadhan Putra Sriwijaya','X',NULL,NULL,'22010283','22010283',NULL,NULL,'Laki-Laki','Batam','2015-07-07','Islam',NULL,'Ds. Kaligoro no.2 RT.10 RW.02 Kel.Pandanmulyo Kec.Pakis Kab.Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081233634417',NULL,NULL,NULL,'Mujib Suyanto',NULL,NULL,NULL,NULL,NULL,'Septanti Ayu Faristin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('56','X NA',NULL,'19010108','19010108','Azka Kenzie Ramdhani','X',NULL,NULL,'19010108','19010108',NULL,NULL,'Laki-Laki','Malang','2012-07-23','Islam',NULL,'Jl. Ki Ageng Gribig Gang 5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'081555651365',NULL,NULL,NULL,'Yudie Wiro',NULL,NULL,NULL,NULL,NULL,'Dina Aulia',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('57','X NA',NULL,'21010232','21010232','Ameera Nayla Rania Winata','X',NULL,NULL,'21010232','21010232',NULL,NULL,'Perempuan','Malang','2014-04-27','Islam',NULL,'Graha Dewata RT 01 RW 12 Merjosari Dau',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0895344222631',NULL,NULL,NULL,'FAMSA WINATA',NULL,NULL,NULL,NULL,NULL,'RENI NOVERA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('58','X NA',NULL,'22010271','22010271','Saifullah Yusuf','X',NULL,NULL,'22010271','22010271',NULL,NULL,'Laki-Laki','Malang','2015-06-01','Islam',NULL,'jl kh malik gang 12 no.34 kedungkandang Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0895414585397',NULL,NULL,NULL,'Imam Kusyairi',NULL,NULL,NULL,NULL,NULL,'Tri Putri Yuniarti',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('59','X NA',NULL,'20010204','20010204','nazneen adlina fathin','X',NULL,NULL,'20010204','20010204',NULL,NULL,'Laki-Laki','Gresik','2013-12-06','Islam',NULL,'Perumahan Karangploso View AA-3 Desa Ngenep Kec. Karangploso Kab Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'085732680209',NULL,NULL,NULL,'M. Faizul Adib',NULL,NULL,NULL,NULL,NULL,'Elistiowaty',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('60','XI NB',NULL,'22020238','22020238','MUHAMMAD IZZUDDIN EL FAKHRI','XI ',NULL,NULL,'22020238','22020238',NULL,NULL,'Laki-Laki','Surabaya','2009-12-22','Islam',NULL,'Tenggilis Mejoyo Utara VI Blok BE No 5, Surabaya',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'08978789500',NULL,NULL,NULL,'Ilyan Bulifar',NULL,NULL,NULL,NULL,NULL,'Irma Rachmadiana',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('61','X NA',NULL,'22010272','22010272','Rubin Imad Lesmana','X',NULL,NULL,'22010272','22010272',NULL,NULL,'Laki-Laki','Malang','2012-03-06','Islam',NULL,'Jl. Tumapel 125, Singosari, Malang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'082171940363',NULL,NULL,NULL,'Riza Yusuf Lesmana',NULL,NULL,NULL,NULL,NULL,'Ima Yuri Tobing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('62','X NA',NULL,'19010113','19010113','Khonsa Khoirunnisa','X',NULL,NULL,'19010113','19010113',NULL,NULL,'Perempuan','Malang','2012-04-15','Islam',NULL,'Jl Panglima Sudirman Gg Manunggal no 11 (pagar putih, sebelah LBB Gold Generation)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'085708589105',NULL,NULL,NULL,'Zakariansyah',NULL,NULL,NULL,NULL,NULL,'Fathiyah Azizah',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('63','X NA',NULL,'20010187','20010187','Abdurrohman Sayyid Kelana Sasmitaning Manah','X',NULL,NULL,'20010187','20010187',NULL,NULL,'Laki-Laki','Kediri','2013-12-05','Islam',NULL,'Jl. Mojowarno No.35 RT.20 RW.03 Kajang Mojorejo Junrejo Kota Batu Jawa Timur',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0881026405258',NULL,NULL,NULL,'Eliya Omayanti',NULL,NULL,NULL,NULL,NULL,'Eliya Omayanti',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('64','X NA',NULL,'21010233','21010233','Qisya Qisthina','X',NULL,NULL,'21010233','21010233',NULL,NULL,'Perempuan','Malang','2013-12-20','Islam',NULL,'Jl Panglima Sudirman Gg Manunggal no 11 (pagar putih, sebelah LBB Gold Generation)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'085708589105',NULL,NULL,NULL,'Zakariansyah',NULL,NULL,NULL,NULL,NULL,'Fathiyah Azizah',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021',NULL,NULL,NULL,NULL,'1','0');
INSERT INTO `siswa` VALUES ('65','X NA',NULL,'22010289','22010289','Conan Hussain Manshurino ','X',NULL,NULL,'22010289','22010289',NULL,NULL,'Laki-Laki','Malang ','2015-07-10','Islam',NULL,'Jl. Sudimoro 18 B RT 2 RW 7 Mojolangu-Lowokwaru-Malang-Jawa Timur 65142',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'(+62) 812523475',NULL,NULL,NULL,'Machbub Junaidi ',NULL,NULL,NULL,NULL,NULL,'Fitri Wulandari ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022',NULL,NULL,NULL,NULL,'1','0');

/*---------------------------------------------------------------
  TABLE: `soal`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `soal`;
CREATE TABLE `soal` (
  `id_soal` int(11) NOT NULL AUTO_INCREMENT,
  `id_mapel` int(11) NOT NULL,
  `nomor` int(5) DEFAULT NULL,
  `soal` longtext DEFAULT NULL,
  `jenis` int(1) DEFAULT NULL,
  `pilA` longtext DEFAULT NULL,
  `pilB` longtext DEFAULT NULL,
  `pilC` longtext DEFAULT NULL,
  `pilD` longtext DEFAULT NULL,
  `pilE` longtext DEFAULT NULL,
  `jawaban` varchar(1) DEFAULT NULL,
  `file` mediumtext DEFAULT NULL,
  `file1` mediumtext DEFAULT NULL,
  `fileA` mediumtext DEFAULT NULL,
  `fileB` mediumtext DEFAULT NULL,
  `fileC` mediumtext DEFAULT NULL,
  `fileD` mediumtext DEFAULT NULL,
  `fileE` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id_soal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*---------------------------------------------------------------
  TABLE: `token`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `id_token` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(6) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `masa_berlaku` time NOT NULL,
  PRIMARY KEY (`id_token`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
INSERT INTO `token` VALUES   ('1','YMEJTU','2023-02-11 17:36:37','00:15:00');

/*---------------------------------------------------------------
  TABLE: `tugas`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `tugas`;
CREATE TABLE `tugas` (
  `id_tugas` int(11) NOT NULL AUTO_INCREMENT,
  `id_guru` int(255) NOT NULL DEFAULT 0,
  `kelas` text NOT NULL,
  `mapel` varchar(255) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `tugas` longtext NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `tgl_mulai` datetime NOT NULL,
  `tgl_selesai` datetime NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_tugas`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
INSERT INTO `tugas` VALUES   ('6','329','a:1:{i:0;s:4:\"X NA\";}','01','Testing','<p>Test</p>',NULL,'2022-12-19 22:04:00','2022-12-20 22:04:00','2022-12-19 22:06:07',NULL);
INSERT INTO `tugas` VALUES ('7','1','a:1:{i:0;s:4:\"X NA\";}','01','Materi Berhitung 1 - 10','<p><a href=\"https://docs.google.com/forms/d/e/1FAIpQLScFLwxlFQkNYyq3WHKjrjo5XyfvYoyvXYwXoPEYsIIEHA4rpQ/viewform?usp=sf_link\" target=\"_blank\">Testing</a><br></p>',NULL,'2023-01-29 13:49:00','2023-01-30 13:49:00','2023-01-29 13:50:39',NULL);
INSERT INTO `tugas` VALUES ('9','344','a:1:{i:0;s:4:\"X NA\";}','01','Jauh Dekat Bisa Didapat','<p>2 + 4 </p>',NULL,'2023-02-11 14:00:00','2023-02-11 17:00:00','2023-02-11 16:51:57',NULL);
INSERT INTO `tugas` VALUES ('10','344','a:1:{i:0;s:4:\"X NA\";}','01','Vektor dan Operasinya ','<p>3 + 7 foto</p>',NULL,'2023-02-11 16:00:00','2023-02-11 17:00:00','2023-02-11 16:53:32',NULL);
INSERT INTO `tugas` VALUES ('11','332','a:1:{i:0;s:4:\"X NB\";}','01','PKN modul 4 Kelas 8','<p>buatlah ringkasan materi modul 4, kirim file berupa document atau link gdrive.</p>',NULL,'2023-02-12 19:09:00','2023-02-18 19:09:00','2023-02-12 19:10:23',NULL);
INSERT INTO `tugas` VALUES ('12','334','a:18:{i:0;s:5:\"semua\";i:1;s:4:\"X NA\";i:2;s:4:\"X NB\";i:3;s:4:\"X TA\";i:4;s:4:\"X TB\";i:5;s:5:\"XI NA\";i:6;s:5:\"XI NB\";i:7;s:5:\"XI NC\";i:8;s:5:\"XI TA\";i:9;s:5:\"XI TB\";i:10;s:5:\"XI TC\";i:11;s:6:\"XII NA\";i:12;s:6:\"XII NB\";i:13;s:6:\"XII NC\";i:14;s:6:\"XII ND\";i:15;s:6:\"XII TA\";i:16;s:6:\"XII TB\";i:17;s:6:\"XII TC\";}','01','Uji Coba E-Learning PKBM Zamzam Kota Malang','<p>Halo! Untuk berlatih mengerjakan dan mengumpulkan tugas, silahkan menjawab kolom uji coba ini dengan:</p><p>1. Tuliskan kesan pertama kalian secara singkat mengenai E-learning PKBM Zamzam Kota Malang</p><p>2. Coba unggah gambar/file apapun di kolom \"File Pendukung\"</p><p>Terima kasih, semangat belajar!</p>',NULL,'2023-02-12 19:00:00','2023-02-19 00:00:00','2023-02-12 19:21:02',NULL);

/*---------------------------------------------------------------
  TABLE: `ujian`
  ---------------------------------------------------------------*/
DROP TABLE IF EXISTS `ujian`;
CREATE TABLE `ujian` (
  `id_ujian` int(11) NOT NULL AUTO_INCREMENT,
  `kode_nama` varchar(255) DEFAULT '0',
  `id_pk` varchar(255) NOT NULL,
  `id_guru` int(5) NOT NULL,
  `id_mapel` int(5) NOT NULL,
  `kode_ujian` varchar(30) DEFAULT NULL,
  `nama` varchar(100) NOT NULL,
  `jml_soal` int(5) NOT NULL,
  `jml_esai` int(5) NOT NULL,
  `bobot_pg` int(5) NOT NULL,
  `opsi` int(1) NOT NULL,
  `bobot_esai` int(5) NOT NULL,
  `tampil_pg` int(5) NOT NULL,
  `tampil_esai` int(5) NOT NULL,
  `lama_ujian` int(5) NOT NULL,
  `tgl_ujian` datetime NOT NULL,
  `tgl_selesai` datetime NOT NULL,
  `waktu_ujian` time DEFAULT NULL,
  `selesai_ujian` time DEFAULT NULL,
  `level` varchar(5) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `sesi` varchar(1) DEFAULT NULL,
  `acak` int(1) NOT NULL,
  `token` int(1) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `hasil` int(1) DEFAULT NULL,
  `kkm` varchar(128) DEFAULT NULL,
  `ulang` int(2) DEFAULT NULL,
  `soal_agama` varchar(50) DEFAULT NULL,
  `reset` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_ujian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
